
package idt;

import java.util.ArrayList;
import java.util.Calendar;
import java.security.SecureRandom;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
/*
 * Simple Object that parse a setting file
 */
public class IdtParser {
	public IdtParser()
	{
            map = new ArrayList<String[]>();
	}
    /**
     * Set value of a key
     * @param data
     * Value to set to a key
     * @param key 
     * Key of the value<br />
     * @return
     * false if the key exist
     */
    public boolean setValueof(String data,String key)
    {            
    	for(int i=0;i<map.size();i++)
    	{
    		if(map.get(i)[0].compareToIgnoreCase(key)==0)
    			return false;
    	}
        String[] record=new String[2];
        record[0]=key;
        record[1]=data;
        map.add(record);
        return true;
    }
    /**
     * Get the string of setting file
     * @return 
     * 
     */
    public String getSettings()
    {
        String nl = System.getProperty("line.separator");
        String data="<!-- idt Setting file version 0.2 -->";
        data+=nl;
        for(int i=0;i<map.size();i++)
        {
            data = String.format("%s<%s>%s</%s>%s",data,map.get(i)[0],map.get(i)[1],map.get(i)[0],nl);
        }
        return data;
    }
    /**
     * Return an ArrayList of Keys for the setting file
     * @return
     */
    public ArrayList<String> getKeys()
    {
    	ArrayList<String> retData = new ArrayList<String>();
    	for(int i=0;i<map.size();i++)
    	{
    		retData.add(map.get(i)[0]);
    	}
    	return retData;
    }
    /**
     * Get database on key
     * @param key
     * @return 
     */
    public String getDataFromKey(String key)
    {
        for(int i=0;i<map.size();i++)
        {
            if(map.get(i)[0].compareToIgnoreCase(key)==0)
            {
                return map.get(i)[1];
            }
        }
        return null;
    }
	/**
	 * Get a value of setting from a string (data) that 
	 * enclosed by tokenBegin and tokenEnd
	 */
	public String getValueof(String data,String tokenBegin,String tokenEnd)
    {
        try{
            data = data.toLowerCase();
            tokenBegin = tokenBegin.toLowerCase();
            tokenEnd = tokenEnd.toLowerCase();
            int i = data.indexOf(tokenBegin);
            int j = data.indexOf(tokenEnd);
            char dst[];
            dst = new char[j-i-tokenBegin.length()];
            data.getChars(i+tokenBegin.length(),j,dst,0);
            String rtVal = new String(dst);
            return rtVal;
        }
        catch(Exception e)
        {
            e.printStackTrace();//Print Debug error information to console
            return null;//Return null so that go back to default setting
        }
    }
	/**
	 * Update the setting value base on key
	 * @param key
	 * @param data
	 * @return
	 * true if data updated
	 */
	public boolean updateData(String key,String data)
	{
		boolean found = false;
		for(int i=0;i<map.size();i++)
		{
			if(map.get(i)[0].compareToIgnoreCase(key)==0)
			{
				map.get(i)[1]=data;
				found=true;
			}
		}
		return found;
	}
	/**
	 * Remove a setting data base on it's key
	 * @param key
	 * @return
	 * true if key removed
	 */
	public boolean removeKey(String key)
	{
		boolean found=false;
		for(int i=0;i<map.size();i++)
		{
			if(map.get(i)[0].compareToIgnoreCase(key)==0)
			{
				map.remove(i);
				found=true;
			}
		}
		return found;
	}
    /**
     * Set data from String to parser structure
     * @param data 
     */
    public void setData(String data)
    {
    	map = null;
        System.gc();
        map = new ArrayList<String[]>();
        parseStruct(data);
    }
    /**
     * Parse setting file structure
     * @param data
     */
    private void parseStruct(String data)
    {
        String key;
        int ki=data.indexOf("<");
        int kj = data.indexOf(">");
        if(ki>-1)
        {
            key = data.substring(ki+1,kj);
            if(key.charAt(0)!='!')
            {
            	this.setValueof(getValueof(data,"<"+key+">","</"+key+">"), key);
            	data=data.substring(kj+1);
            	kj = data.indexOf('>');
            }
        	parseStruct(data.substring(kj+1));
        }
    }
    /**
     * Get a secure random key to encode the data
     * @return
     */
    static public char getRandomKey()
    {
    	SecureRandom rand = new SecureRandom();
    	rand.setSeed(Calendar.getInstance().getTimeInMillis());
    	return (char)(rand.nextInt());
    }
    /**
     * Encode a data by key
     * @param data
     * data to encode
     * @param key
     * key of encoding
     * @return 
     */
    static public String encode(String data,char key)
    {
        byte[] passwd=new byte[data.length()+1];
        passwd[0] = (byte)(key);
        String ret = "";
        try
        {
            for(int i=0;i<data.length();i++)
            {
                passwd[i+1] = (byte)(key ^ data.charAt(i));
            }
            for(int i=0;i<passwd.length;i++)
            {
                ret += String.format("%03d", (passwd[i]&0x00ff));
            }
        }
        catch(Exception e)
        {
            try
            {
                throw e;
            }
            catch(Exception exp)
            {
                exp.printStackTrace();
            }
        }
        return ret;
    }
    /**
     * Decode the data
     * @param data
     * @return 
     */
    static public String decode(String data)
    {
        char[] passwd=new char[(data.length()/3)-1];
        String tmp = data.substring(0, 3);
        char key = (char)(Integer.parseInt(tmp));
        try
        {
            for(int i=0;i<passwd.length;i++)
            {
                tmp = data.substring((i+1)*3, (i+2)*3);
                passwd[i] = (char)(key ^ Integer.parseInt(tmp));
            }
        }
        catch(Exception e)
        {
            try
            {
                throw e;
            }
            catch(Exception exp)
            {
                exp.printStackTrace();;
            }
        }
        return new String(passwd);
    }
    static public String decodeV2(String data)
    {
    	byte[] retData;
    	String temp = data.substring(data.length()-4);
    	int keyLen = Integer.parseInt(temp,16);
    	int pos = data.length()-keyLen*3;
    	byte[] key = new byte[keyLen];
    	retData = new byte[pos-4];
    	for(int i=0;i<keyLen;i++)
    	{
    		key[i] = (byte)Integer.parseInt(data.substring(data.length()-((keyLen-i)*3)-4,data.length()-((keyLen-i)*3)-4+3));
    	}
    	data = data.substring(0,(pos)-4);
    	for(int i=0;i<data.length()/3;i++)
    	{
    		temp = data.substring(i*3,(i+1)*3);
    		retData[i] = (byte)(Integer.parseInt(temp)^key[i%keyLen]);
    	}
    	BufferedInputStream is = new BufferedInputStream(new java.io.ByteArrayInputStream(retData));
    	java.io.BufferedReader bs = new BufferedReader(new java.io.InputStreamReader(is)); 
    	String ret = new String();
    	String line = new String();
    	String lineSeparator="\n\r";
    	try
		{
	    	while((line=bs.readLine())!=null)
	    	{
	    			if(line.length()>0)
	    			{
			    		ret+=(line+lineSeparator);
	    			}
	    			line=null;
    		}
		}
    	catch(Exception e)
		{
			try
			{
				throw e;
			}
			catch(Exception exp)
			{
				exp.printStackTrace();
			}
    	}
    	return ret;
    }
    static public String encode(String data,int keyLen)
    {
    	String retData=new String();
    	byte[] key = new byte[keyLen];
    	SecureRandom rand = new SecureRandom();
    	rand.setSeed(Calendar.getInstance().getTimeInMillis());
    	rand.nextBytes(key);
    	BufferedInputStream is = new BufferedInputStream(new java.io.ByteArrayInputStream(data.getBytes()));
    	int bData;
    	int i=0;
    	try
    	{
	    	while((bData = is.read())>0)
	    	{
	    		retData += String.format("%03d",(bData ^ key[i%keyLen])&0xff);
	    		i++;
	    	}
	    	for(i=0;i<keyLen;i++)
	    	{
	    		retData = String.format("%s%03d",retData,( key[i]&0xff));
	    	}
	    	retData = String.format("%s%04x",retData, keyLen);
    	}
    	catch(Exception e)
    	{
    		try
    		{
    			throw e;
    		}
    		catch(Exception exp)
    		{
	    			exp.printStackTrace();
    		}
    	}
    	return retData;
    }
    private ArrayList<String[]> map;
}
