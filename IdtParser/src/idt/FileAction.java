package idt;

import java.io.*;
/*
 * @Basic Function on read and write text file
 */
public class FileAction {
	private String flName;
    /*
     * @ Creates a new instance of FileAction by the name n
     */
    public FileAction(String n) {
        flName = n;
    }
    /*
     * @Write the text to file
     */
    public boolean writeFile(String data)
    {
        boolean rtVal=true;
        try{
        	//Code to write to file
            String text = data;
            byte b[] = text.getBytes();            
            File outputFile = new File(flName);
            FileOutputStream out = new 
                    FileOutputStream(outputFile);
            out.write(b);
            out.close();
        }catch(Exception e)
        {
            System.out.println(e.getMessage());
            rtVal = false;
        }
        return rtVal;
    }
    /* 
     * @Read s string from file
     */
    public String readFile() 
    {
        String data = new String();
        try{
            File inputFile = new File(flName);
            FileInputStream in = new 
                  FileInputStream(inputFile);
            byte bt[] = new 
                  byte[(int)inputFile.length()];
            in.read(bt);
            data = new String(bt);
            in.close();
        }catch(java.io.IOException e){
            System.out.println(e.getMessage());    
        }
        return data;
     }

}
