package idt;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;


public class CsvReader {
	private String rec;
	private FileInputStream fis;
	private FileOutputStream fos;
	private BufferedReader br;
	private BufferedWriter bw;
	public CsvReader()
	{
	}
	/**
	 * Split CSV data to ArrayList
	 * @param csv
	 * @return
	 */
	public ArrayList<String>getData(String csv)
	{
		ArrayList<String> data = new ArrayList<String>();
		int comaPos=0;
		while(csv.length()>0)
		{
			comaPos = csv.indexOf(",");
			if(comaPos>0)
			{
				data.add(csv.substring(0,comaPos));
				csv = csv.substring(comaPos+1);
			}
			else
			{
				data.add(csv);
				csv="";
			}
		}
		return data;
	}
	/**
	 * create a file
	 * @param filename
	 * Name of the file
	 */
	public void createFile(String filename)
	{
		try
		{
			fos = new FileOutputStream(filename);
			DataOutputStream out = new DataOutputStream(fos);
			bw = new BufferedWriter(new OutputStreamWriter(out));
		}
		catch(Exception e)
		{
			try
			{
				throw e;
			}
			catch(Exception exp)
			{
				exp.printStackTrace();
			}
		}
	}
	/**
	 * Write a line to file
	 * @param data
	 */
	public void writeLine(String data)
	{
		try
		{
			bw.write(data);
			bw.newLine();
			bw.flush();
		}
		catch(Exception e)
		{
			try
			{
				throw e;
			}
			catch(Exception exp)
			{
				exp.printStackTrace();
			}
		}
	}
	/**
	 * Open the file and prepare to read
	 * @param fileName
	 */
	public void openfile(String fileName)
	{
		try
		{
			fis = new FileInputStream(fileName);
			DataInputStream in = new DataInputStream(fis);
			br = new BufferedReader(new InputStreamReader(in));
		}
		catch(Exception e)
		{
			try
			{
				throw e;
			}
			catch(Exception exp)
			{
				exp.printStackTrace();
			}
		}
	}
	/**
	 * Read a single line from file
	 * @return
	 */
	public String getLine()
	{
		try
		{
			String rec = br.readLine();
			return rec;
		}
		catch(Exception e)
		{
			try
			{
				throw e;
			}
			catch(Exception exp)
			{
				exp.printStackTrace();
			}
			return null;
		}
	}
	public void close()
	{
		try
		{
			if(fis!=null)
				fis.close();
			if(fos!=null)
				fos.close();
			fis=null;
			fos=null;
			br=null;
			bw=null;
			System.gc();
		}
		catch(Exception e)
		{
			try
			{
				throw e;
			}
			catch(Exception exp)
			{
				exp.printStackTrace();
			}
		}
	}
}
/*
try{
// Open the file that is the first 
// command line parameter
FileInputStream fstream = new FileInputStream("textfile.txt");
// Get the object of DataInputStream
DataInputStream in = new DataInputStream(fstream);
BufferedReader br = new BufferedReader(new InputStreamReader(in));
String strLine;
//Read File Line By Line
while ((strLine = br.readLine()) != null)   {
// Print the content on the console
System.out.println (strLine);
}
//Close the input stream
in.close();
  }catch (Exception e){//Catch exception if any
System.err.println("Error: " + e.getMessage());
}
*/