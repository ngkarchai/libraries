package idt;
import java.util.ArrayList;
import java.util.Calendar;
import java.security.SecureRandom;
/**
 * Class to generate Idt Doc.
 * @author NG Karchai
 * Copyright 2011 NG KarChai
 *
 */
public class IdtDoc {
	private ArrayList<String[]> doc;
	public IdtDoc()
	{
		doc = new ArrayList<String[]>();
	}
	/**
     * Set data from String to parser structure
     * @param data 
     */
    public void setData(String data)
    {
    	doc = null;
        System.gc();
        doc = new ArrayList<String[]>();
        parseStruct(data);
    }
	/**
	 * Parse the document structure
	 * @param data
	 */
	private void parseStruct(String data)
    {
        String key;
        int ki=data.indexOf("<");
        int kj = data.indexOf(">");
        if(ki>-1)
        {
            key = data.substring(ki+1,kj);
            if(key.charAt(0)!='!')
            {
            	this.addData(getValueof(data,"<"+key+">","</"+key+">"), key);
            	data=data.substring(kj+1);
            	kj = data.indexOf('>');
            }
        	parseStruct(data.substring(kj+1));
        }
    }/**
	 * Get a value of setting from a string (data) that 
	 * enclosed by tokenBegin and tokenEnd
	 */
	public String getValueof(String data,String tokenBegin,String tokenEnd)
    {
        try{
            data = data.toLowerCase();
            tokenBegin = tokenBegin.toLowerCase();
            tokenEnd = tokenEnd.toLowerCase();
            int i = data.indexOf(tokenBegin);
            int j = data.indexOf(tokenEnd);
            char dst[];
            dst = new char[j-i-tokenBegin.length()];
            data.getChars(i+tokenBegin.length(),j,dst,0);
            String rtVal = new String(dst);
            return rtVal;
        }
        catch(Exception e)
        {
            e.printStackTrace();//Print Debug error information to console
            return null;//Return null so that go back to default setting
        }
    }
	/**
     * Get the string of setting file
     * @return 
     * 
     */
    public String getDoc()
    {
        String nl = System.getProperty("line.separator");
        String data="<!-- idt Doc file version 0.1 -->";
        data+=nl;
        for(int i=0;i<doc.size();i++)
        {
            data = String.format("%s<%s>%s</%s>%s",data,doc.get(i)[0],doc.get(i)[1],doc.get(i)[0],nl);
        }
        return data;
    }
	/**
     * Add value of a key
     * @param data
     * Value to set to a key
     * @param key 
     * Key of the value<br />
     * @return
     * false if the key exist
     */
    public boolean addData(String data,String key)
    {            
        String[] record=new String[2];
        record[0]=key;
        record[1]=data;
        doc.add(record);
        return true;
    }
	/**
	 * Get a random byte array base on time 
	 * @param len
	 * len of random byte array to generate
	 * @return
	 * Random Byte array
	 */
	public byte[] getRandomBytes(int len)
	{
		Calendar cal = Calendar.getInstance();
		SecureRandom rand = new SecureRandom();
		rand.setSeed(cal.getTimeInMillis());
		byte[] retBytes = new byte[len];
		rand.nextBytes(retBytes);
		return retBytes;
	}
	/**
     * Return an ArrayList of Keys for the doc file
     * @return
     */
    public ArrayList<String> getKeys()
    {
    	ArrayList<String> retData = new ArrayList<String>();
    	for(int i=0;i<doc.size();i++)
    	{
    		retData.add(doc.get(i)[0]);
    	}
    	return retData;
    }
    /**
     * Get database on key
     * @param key
     * @return 
     */
    public String getDataFromKey(String key,int index)
    {
    	int pos=0;
        for(int i=0;i<doc.size();i++)
        {
            if(doc.get(i)[0].compareToIgnoreCase(key)==0)
            {
            	if(pos==index)
            		return doc.get(i)[1];
            	else
            		pos++;
            }
        }
        return null;
    }
    /**
	 * Update the setting value base on key
	 * @param key
	 * @param data
	 * @param index
	 * @return
	 * true if data updated
	 */
	public boolean updateData(String key,String data,int index)
	{
		int pos=0;
		for(int i=0;i<doc.size();i++)
		{
			if(doc.get(i)[0].compareToIgnoreCase(key)==0)
			{
				if(pos==index)
				{
					doc.get(i)[1]=data;
					return true;
				}
			}
		}
		return false;
	}
	/**
	 * Remove a setting data base on it's key
	 * @param key
	 * Reference Key
	 * @param index
	 * @return
	 * true if key removed
	 */
	public boolean removeKey(String key,int index)
	{
		int pos=0;
		for(int i=0;i<doc.size();i++)
		{
			if(doc.get(i)[0].compareToIgnoreCase(key)==0)
			{
				if(pos==index)
				{
					doc.remove(i);
					return true;
				}
			}
		}
		return false;
	}
}
