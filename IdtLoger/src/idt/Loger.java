/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package idt;

/**
 *
 * @author ng
 */
public class Loger {
    /**
     * Create a log object to perform log operation
     * @param logFilePath
     * path to save the log file
     * @param logFileName
     * File Name of the log file, .log extension will added automatically
     * @param maxRecs 
     * Maximum record to save in one file.
     */
    public Loger(String logFilePath,String logFileName,Integer maxRecs)
    {
        this.logFileName = logFileName;
        this.logFilePath = logFilePath;
        this.maxRecs = maxRecs;
    }
    /**
     * 
     * @param data
     * @param mode
     * mode=0 writing in multi thread
     * mode=1 writing in batch mode
     * @return 
     */
    public boolean writeLog(String data,int mode)
    {
        LogFileAction log = new LogFileAction(logFilePath,logFileName,maxRecs);
        log.setLog(data);
        if(mode==0)
            log.start();
        else
            log.batchWrite();
        return true;
    }
    
    private String logFilePath;
    private String logFileName;
    private Integer maxRecs;
}
