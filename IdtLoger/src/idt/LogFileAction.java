/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package idt;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.util.Calendar;

/**
 *
 * @author ng
 */
public class LogFileAction extends Thread{
    /**
     * Create and log file thread
     * @param filePath
     * @param fileName
     * @param maxRecs 
     */
    public LogFileAction(String filePath,String fileName,Integer maxRecs)
    {
        this.logFileName=fileName;
        this.logFilePath=filePath;
        this.maxRecs=maxRecs;
    }
    /**
     * 
     * @param data 
     */
    public void setLog(String data)
    {
        logData = data;
    }
    @Override
    public void run()
    {
        batchWrite();
    }
    public void batchWrite()
    {
        try
        {
            File file = new File(logFilePath+logFileName+".log");
            if(!file.exists())file.createNewFile();
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String data;
            int recNo=0;
            String oldRec="";
            while((data=reader.readLine())!=null)
            {
                if(data.length()>0)
                {
                    data+="\n\r";
                    //System.out.print(data);
                    recNo++;
                    oldRec+=data;
                    if(recNo>=maxRecs)
                    {
                        reader.close();
                        mvFile();
                        oldRec="";
                        file = new File(logFilePath+logFileName+".log");
                        if(!file.exists())file.createNewFile();
                        reader = new BufferedReader(new FileReader(file));
                    }
                }
            }
            Calendar now=Calendar.getInstance();
            data=logData;            
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));
            String logRec = String.format(" %s%04d-%02d-%02d %02d:%02d:%02d %s", 
                    oldRec,
                    now.get(Calendar.YEAR),now.get(Calendar.MONTH)+1,now.get(Calendar.DAY_OF_MONTH),+
                    now.get(Calendar.HOUR),now.get(Calendar.MINUTE),now.get(Calendar.SECOND)
                    ,data);
            writer.write(logRec);    
            //writer.newLine();
            writer.flush();
            writer.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
    private void mvFile()
    {
        Calendar date=Calendar.getInstance();
        File file = new File(logFilePath+logFileName+".log");
        file.renameTo(new File(logFilePath+logFileName+
                date.get(Calendar.YEAR) 
                +date.get(Calendar.MONTH) 
                +date.get(Calendar.DAY_OF_MONTH)
                +date.get(Calendar.HOUR_OF_DAY) 
                +date.get(Calendar.MINUTE)
                +date.get(Calendar.SECOND)
                + ".log"));
    }
    private String logFilePath;
    private String logFileName;
    private Integer maxRecs;
    private String logData;
}
